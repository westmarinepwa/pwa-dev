import React, { Component } from 'react';
import { render } from 'react-dom';
// Import routing components
//import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';
import Home from './common/home.component.js'
import Dashboard from './common/dashboard.component.js'
class App extends Component {

  render() {
    return (
	<Router>
        <Route>
           <Switch>
				<Route exact path='/' component={Home} />
                  <Route exact path='/dashboard' component={Dashboard} />
                  
            </Switch>
        </Route>
    </Router>   
    );
  }
}
export default App;






