import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
class Dashboard extends Component {
    render(){
        return (
		
		<nav className="navbar fixed-top navbar-fixed-top">
			<div className="header-container">
				<header className="header wm-header">
					
					<div className="menu-collps">
						<img className="hambrgr-collps" src="images/hamb.jpg"/>
						<img className="hambrgr-epnd hide" src="images/hamb.jpg" />
					</div>
					<div className="logo">
						<a className="site-logo" href="/dashboard" title="site logo">
							<img title="logo" src="images/wm-logo.jpg"/>
						</a>
					</div>
					
					<div className="hdr-right">
						<a className="cart-logo" href="#" title="cart logo">
							<img title="logo" src="images/cart.jpg"/>
						</a>
						
						<a className="search-icon pull-right" href="#" title="search">
							<img title="logo" src="images/search-icon.png"/>
						</a>
					</div>
				</header>
			</div>
			
			<div className="menu-epnd-content hide">
			
			<div className="user">
				<img src="images/user.jpg"/>
				<a className="user-nm">Brooklyn Decker</a>
			</div>
				
			<ul className="menu">
				<li><a title="Store Locator" href="#">
						<img src=""/>
						<span>Store Locator</span>
					</a>
				</li>
				<li><a title="Sales" href="#">
						<img src=""/>
						<span>Sales</span>
					</a>
				</li>
				<li><a title="New Products" href="#">
						<img src=""/>
						<span>New Products</span>
					</a>
				</li>
				<li><a title="Water Sports" href="#">
						<img src=""/>
						<span>Water Sports</span>
					</a>
				</li>
				<li><a title="Fishing" href="#">
						<img src=""/>
						<span>Fishing</span>
					</a>
				</li>
				<li><a title="Boats, Motors & Parts" href="#">
						<img src=""/>
						<span>Boats, Motors & Parts</span>
					</a>
				</li>
				<li><a title="Safety" href="#">
						<img src=""/>
						<span>Safety</span>
					</a>
				</li>
				<li><a title="Sailing" href="#">
						<img src=""/>
						<span>Sailing</span>
					</a>
				</li>
				<li><a title="Plumbing & Vantilation" href="#">
						<img src=""/>
						<span>Plumbing & Vantilation</span>
					</a>
				</li>
				<li><a title="Maintainance & Hardware" href="#">
						<img src=""/>
						<span>Maintainance & Hardware</span>
					</a>
				</li>
				<li><a title="Logout" href="#">
						<img src=""/>
						<span>Logout</span>
					</a>
				</li>
			</ul>
			</div>
			
			<div className="search-bar hide">
				<div className="search-txt-bar">
					<div className="form-group has-feedback">
						<input type="text" className="form-control" id="inputValidation" placeholder="Search keyword, Supplies etc.."/>
						<span className="glyphicon glyphicon-search form-control-feedback"></span>
					</div>
				</div>
				
				<div className="search-inputs">
					<img src="images/img.jpg"/>
					<img src="images/img.jpg"/>
				</div>
			
			</div>
			 <Carousel showThumbs={false} interval = {10000} autoPlay ={true} showArrows={false} showStatus={false} showIndicators={false} infiniteLoop={false}>
               <div>
                   <img src="images/slider1.png" />
                  
               </div>
               <div>
                   <img src="images/slider2.png" />
                  
               </div>
               <div>
                   <img src="images/slider3.png" />
                   
               </div>
           </Carousel>
			<div className="box-carousel">
				<div className="">
					<div className="" id="previous">
					
					</div>
					<div className="" id="next"></div>
				</div>
				 <div className="">
				  <img src="images/tile-back.png" alt="" title=""/>
						<div className="box1 box">
							<img src=""/>
							<span id="1">text1</span>
						
						</div> 
						<div className="box2 box">
							<img src=""/>
							<span id="2">text2</span>
						
						</div>
						<div className="box3 box">
							<img src=""/>
							<span id="3">text3</span>
						
						</div>
						<div className="box4 box">
							<img src=""/>
							<span id="4">text4</span>
						
						</div>
				  </div>
			</div>
		
		</nav>

		
		
		);
    }
}
export default Dashboard