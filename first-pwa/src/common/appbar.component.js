import React from 'react';
import AppBar from 'material-ui/AppBar';
import DrawerLeft from './drawerleft.component.js';

export default class AppBarTop extends React.Component {

   /* constructor() {
        this.state = {
            open: false
        }
    }  
	*/
    //Toggle function (open/close Drawer)
    toggleDrawer() {
        this.setState({
            open: !this.props.open
        })
    }

    render() {
        return (
            <div>
                <AppBar
                    title="Title"
                    onLeftIconButtonTouchTap={this.toggleDrawer.bind(this)} 
                />
                <DrawerLeft open={this.props.open} onToggleDrawer={this.toggleDrawer.bind(this)} />
           </div>
        )
    }
}