import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import { Drawer, AppBar, MenuItem,Menu} from 'material-ui'
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import * as Colors from 'material-ui/styles/colors';
import FlatButton from 'material-ui/FlatButton';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Divider from 'material-ui/Divider';
injectTapEventPlugin();

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#0D63A8',
    primary1Color: '#0D63A8',
    primary2Color: Colors.indigo700,
    accent1Color: Colors.redA200,
    pickerHeaderColor: Colors.darkBlack,
  },
  appBar: {
    height: 60,
  },
});



class Dashboard extends Component {
  constructor(props) {
    super(props);
	this.toggleIconimage = '';
    this.state = {open: false};
  }

  handleToggle = () => this.setState({open: !this.state.open});
  handleClose() { this.setState({open: false});
  document.getElementById('toggleIcon').src = 'images/menu-icon.svg'; }
	changeIcon(){
		this.toggleIconimage  = document.getElementById('toggleIcon').src;
		alert(this.toggleIconimage);
	}
    render(){
        return (
<div className="home-page">
		<nav className="navbar fixed-top navbar-fixed-top">

			<div className="search-bar hide">
				<div className="search-txt-bar">
					<div className="form-group has-feedback">
						<input type="text" className="form-control" id="inputValidation" placeholder="Search keyword, Supplies etc.."/>
						<span className="glyphicon glyphicon-search form-control-feedback"></span>
					</div>
				</div>

				<div className="search-inputs">
					<img src="images/img.jpg"/>
					<img src="images/img.jpg"/>
				</div>

			</div>


            <MuiThemeProvider muiTheme={muiTheme}>
                         <div classNmae="drwr-wrap">

                            <Drawer
						
                               containerStyle={ {'top': '60px'} }
                               docked={true}
                               open={this.state.open}>

								<div className="user">
									<img src="images/user.jpg"/>
									<a className="user-nm">Brooklyn Decker</a>
							    </div>

							    <div className="List menu-nav">
								<Menu desktop={true} style={styleDivider}>

                               <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/location-icon.svg" style={imageStyle}/>Store Locator </MenuItem>
							   <Divider />

							   <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/sale-icon.svg" style={imageStyle}/>Sales </MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/star-icon-gray.svg" style={imageStyle}/>New Products </MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/water-sport-gray.svg" style={imageStyle} />Water Sports </MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/fishing-icon-gray.svg" style={imageStyle}/>Fishing </MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/motor1-icon.svg" style={imageStyle}/>Boats, Motors and Parts </MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/safety-icon-gray.svg" style={imageStyle}/>Safety </MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/sailing-icon-gray.svg" style={imageStyle}/>Sailing </MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle}onTouchTap={this.handleClose.bind(this)}><img src="images/plumbing-icon.svg" style={imageStyle}/>Plumbing and Vantilation</MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle} onTouchTap={this.handleClose.bind(this)}><img src="images/maintainance-icon.svg" style={imageStyle}/>Maintainance & Hardware</MenuItem>
                               <Divider />
							   <MenuItem style={menuTextStyle}  onTouchTap={this.handleClose.bind(this)}><img src="images/logout-icon.svg" style={imageStyle}/>Logout </MenuItem>
                                
								</Menu>
								</div>
                            </Drawer>

                            <AppBar
								title={<center><img style={logostyle}  src="images/logo2.png"/><span className="wm-logo-txt">West marine</span></center>}
								


                                   isInitiallyOpen={true} onLeftIconButtonTouchTap={this.handleToggle} onLeftIconButtonClick={this.handleToggle}
                                   iconElementRight={
                                       <div >
                                          <img style={divStyle1} src="images/cart-icon.svg"/>
                                          <img style={divStyle} src="images/search-icon.svg"/>
										  
                                       </div>
								   }
								   iconElementLeft={
									<div onLeftIconButtonTouchTap={this.handleToggle} onLeftIconButtonClick={this.handleToggle}>
									 <img style={divStyle1} src="images/menu-icon.svg" id ="toggleIcon"/>
									 </div>  
									 
								 }
								   
                                 
                             />
               </div>    
             </MuiThemeProvider >
    </nav>
	<div className="main-content">
		 <Carousel showThumbs={false} interval = {10000} stopOnHover = {true} autoPlay ={true} showArrows={false} showStatus={false} showIndicators={false} infiniteLoop={true}>
			   <div>
				   <img src="images/slider1.png" />
				  
			   </div>
			   <div>
				   <img src="images/slider2.png" />
				  
			   </div>
			   <div>
				   <img src="images/slider3.png" />
				   
			   </div>
		</Carousel>
		<div className="box-carousel">
					<div className="carousel-arrow">
						<div className="left" id="next">
							<img src="images/arrow-left.svg" />
						</div>
						<div className="right" id="previous">
							<img src="images/arrow-right.svg" />
						</div>

					</div>
					 <img src="images/tile-back.png" alt="" title=""/>
					 <div className="carousel-data" data-role="page" id="pageone">
					 
							<div className="box1 box" data-role="main">
								<img id = "image1" src="images/watersport-icon.svg"/>
								<span id="1">Water Sports</span>
							
							</div> 
							<div className="box2 box" data-role="main">
								<img id = "image2" src="images/motors-icon.svg"/>
								<span id="2">Boats Motor & Parts</span>
							
							</div>
							<div className="box3 box" data-role="main">
								<img id = "image3" src="images/fishing-icon.svg"/>
								<span id="3">Fishing</span>
							
							</div>
							<div className="box4 box" data-role="main">
								<img src="images/star-icon.svg"/>
								<span id="4">New Products</span>
							
							</div>
					  </div>
				</div>
	</div>

</div>

		);
    }
}
const styleDivider = {
	float: 'left',
  };

const divStyle = {
  width: 35,
  height: 35,
  margin: '10px',
  height: 30,
};
const divStyle1 = {
	margin: '10px',
	height: 30,
  };

const imageStyle = {
    width: 25,
    height: 25,
	 marginRight : '10px',
	// marginLeft: '10px',
};

const menuTextStyle = {
    fontSize:17,
    textAlignVertical : 'auto',
	border: '5px',
	

};


const logoStyle = {
    height: 40,
};
const logostyle ={
	height :40,
	marginLeft :'40px',
}


export default Dashboard