import React from 'react';
import AppBar from 'material-ui/AppBar';
import DrawerLeft from './DrawerLeft.jsx';

export default class Header extends React.Component {

    constructor() {
        this.state = {
            open: false
        }
    }  
    //Toggle function (open/close Drawer)
    toggleDrawer() {
        this.setState({
            open: !this.state.open
        })
    }

    render() {
        return (
            <div>
                <AppBar
                    title="Title"
                    onLeftIconButtonTouchTap={this.toggleDrawer.bind(this)} 
                />
                <DrawerLeft open={this.state.open} onToggleDrawer={this.toggleDrawer.bind(this)} />
           </div>
        )
    }
}