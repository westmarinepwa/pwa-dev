import React from 'react';
import AppBar from 'material-ui/AppBar';

import DrawerLeft from './DrawerLeft.jsx';

function handleTouchTap() {
    // Tried it her 
}

const AppBarTop = () => (
   <div>
        <AppBar
            title="Title"
            onLeftIconButtonTouchTap={handleTouchTap}
        />
        <DrawerLeft />
   </div>
);

export default AppBarTop;