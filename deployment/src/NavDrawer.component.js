import React, { Component } from 'react';
import { Drawer, AppBar, MenuItem, Menu } from 'material-ui'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import * as Colors from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import PropTypes from 'prop-types';
import SpeechRecognition from 'react-speech-recognition';

var router = require('react-router');

const muiTheme = getMuiTheme({
    palette: {
        textColor: '#0D63A8',
        primary1Color: '#0D63A8',
        primary2Color: Colors.indigo700,
        accent1Color: Colors.redA200,
        pickerHeaderColor: Colors.darkBlack,
    },
    appBar: {
        height: 60,
    },
});

const style = {
    marginLeft: 10,
    marginRight: 10,
};
var recognition = new window.webkitSpeechRecognition

recognition.continous = false
recognition.interimResults = true
recognition.lang = 'en-US'

class NavDrawer extends Component {

    constructor(props) {
        super(props);
        this.toggleIconimage = '';
        this.state = { open: false };
        this.onProdDetail = this.onProdDetail.bind(this);
        this.goBack = this.goBack.bind(this);
        this.state = {
            listening: false,
            showResults: true,
            recognizing: false,
        }
        this.toggleListen = this.toggleListen.bind(this)
        this.handleListen = this.handleListen.bind(this)
        this.play = this.play.bind(this)

        this.updateUI = this.updateUI.bind(this)
    }

    goBack(e) {
        e.preventDefault();
        var back = localStorage.getItem('back')
        if (back == 'No') {
        }
        else {
            this.context.router.goBack();
        }
    }

    onProdDetail(e) {
        e.preventDefault();
        localStorage.setItem('back', 'Yes')
        this.context.router.push('/Products');
        // this.props.history.push('/Products');
    }

    handleToggle = () => this.setState({ open: !this.state.open });
    handleClose() {
        this.setState({ open: false });
        document.getElementById('toggleIcon').src = 'images/menu-icon.svg';
        document.getElementById('toggleIcon').alt = 'menuIcon';
    }

    toggleListen() {
    
        // console.log("--------------toggleListen------------", this.state.listening);
        this.refs.interim.value = "";
        this.setState({
            listening: !this.state.listening,
            showResults: !this.state.showResults,
        }, this.handleListen)
    }

    updateUI() {
        if (this.refs.searchRef) {
            this.refs.interim.value = "";
            this.setState({
                listening: false,
                showResults: true,
                recognizing: false,
            }, this.handleListen)
            // console.log("--------------update UI after--------------", this.state.listening);
        }
    }

    play() {
        if (this.state.listening) {
            // console.log("timeout");
            this.setState({
                listening: !this.state.listening,
                showResults: !this.state.showResults,
                recognizing: false,
            }, this.handleListen)
        }
    }

    handleListen() {
        // e.preventDefault();
        // console.log('listening?', this.state.listening)

        if (this.state.listening) {

            if (!this.state.recognizing) {
                // console.log('listening?', "--------recognizing-----------" + this.state.recognizing)
                try{
                recognition.start()
                }catch(err){
                }
            }
            recognition.onend = () => {
                // console.log("...continue listening...", this.state.recognizing)
                recognition.start()
            }
        } else {
            recognition.stop()
            recognition.onend = () => {
                this.setState({ recognizing: false, });
                // console.log("Stopped listening per click", this.state.recognizing)
            }
        }

        let finalTranscript = ''
        recognition.onresult = event => {
            let interimTranscript = ''

            for (let i = event.resultIndex; i < event.results.length; i++) {
                const transcript = event.results[i][0].transcript;
                interimTranscript += transcript;
            }
            // console.log("interim-----------", interimTranscript);
            this.refs.interim.value = interimTranscript;

        }

        recognition.onspeechend = () => {
            this.setState({ recognizing: false, });
            // console.log('onspeechend', this.state.listening);
            // console.log('----------speechend----------------', this.state.recognizing);
            setTimeout(
                this.play
                , 2000);
        }

        recognition.onerror = event => {
            // console.log("Error occurred in recognition: " + event.error)
            this.setState({
                listening: !this.state.listening,
                showResults: !this.state.showResults,
                recognizing: false,
            }, this.handleListen)
        }

        recognition.onstart = event => {
            this.setState({ recognizing: true, });
            // console.log(" ----On Start---------" + this.state.recognizing)
        };
        recognition.onend = event => {
            this.setState({ recognizing: false, });
            // console.log(" ----On End---------" + this.state.recognizing)
        };
    }

    render() {
        return (
            <div className="home-page">
                <nav className="navbar fixed-top navbar-fixed-top">

                    <div className="search-bar hide">
                        <div className="search-txt-bar">
                            <div className="form-group has-feedback">
                                <input type="text" className="form-control" id="inputValidation" placeholder="Search keyword, Supplies etc.." />
                                <span className="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>

                        <div className="search-inputs">
                            <img src="images/img.jpg" alt="" />
                            <img src="images/img.jpg" alt="" />
                        </div>

                    </div>
                    <MuiThemeProvider muiTheme={muiTheme}>
                        <div className="drwr-wrap">

                            <AppBar
                                title={<center><img style={logostyle} src="images/logo2.png" alt="logo" />
                                    <span className="wm-logo-txt">West Marine</span></center>}

                                isInitiallyOpen='true' onTitleClick={this.goBack} className="app-nav" onLeftIconButtonClick={this.handleToggle}
                                iconElementRight={
                                    <div>
                                        <img style={divStyle1} src="images/cart-icon.svg" alt="" />
                                        <img style={divStyle} src="images/search-icon.svg" className="search" id="toggleSearch" alt="toggleCross" onClick={this.updateUI} />

                                    </div>
                                }
                                iconElementLeft={
                                    <div>
                                        <img style={divStyle1} src="images/menu-icon.svg" id="toggleIcon" alt="menuIcon" />
                                    </div>

                                }
                            />
                            <Drawer
                                containerStyle={{ 'top': '85px' }}
                                docked={false}

                                open={this.state.open} width={320}
                                onDrawerOpen={() => {
                                    console.log("Drawer is opened !");
                                }}
                                onDrawerClose={() => {
                                    console.log("Drawer is closed !");
                                    //this.props.dispatch(displayDrawer());
                                }}
                                disableSwipeToOpen={true}
                                onRequestChange={() => {
                                    console.log("Drawer is closed request !");
                                    this.handleClose();
                                    //this.props.dispatch(displayDrawer());
                                }}
                            >
                                <div className="user">
                                    <img src="images/user1.jpg" alt="" />
                                    <a className="user-nm">Brooklyn Decker</a>
                                </div>

                                <div className="List menu-nav">
                                    <Menu desktop={true} width={320} style={styleDivider}>

                                        <MenuItem className="menu-item" eventKey="2" style={menuTextStyle} onClick={this.onProdDetail}><img src="images/sale-icon.svg" alt="" style={imageStyle} />Sales </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="1" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/location-icon.svg" alt="" style={imageStyle} />Store Locator </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="3" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/star-icon-gray.svg" alt="" style={imageStyle} />New Products </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="4" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/water-sport-gray.svg" alt="" style={imageStyle} />Water Sports </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="5" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/fishing-icon-gray.svg" alt="" style={imageStyle} />Fishing </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="6" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/motor1-icon.svg" alt="" style={imageStyle} />Boats, Motors and Parts </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="7" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/safety-icon-gray.svg" alt="" style={imageStyle} />Safety </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="8" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/sailing-icon-gray.svg" alt="" style={imageStyle} />Sailing </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="9" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/plumbing-icon.svg" alt="" style={imageStyle} />Plumbing and Vantilation</MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="10" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/maintainance-icon.svg" alt="" style={imageStyle} />Maintainance & Hardware</MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="11" style={menuTextStyle} onClick={this.handleClose.bind(this)}><img src="images/logout-icon.svg" alt="" style={imageStyle} />Logout </MenuItem>
                                        <Divider style={style} />
                                        <MenuItem className="menu-item" eventKey="12" style={menuTextStyle} onClick={this.handleClose.bind(this)}></MenuItem>
                                        <MenuItem className="menu-item" eventKey="13" style={menuTextStyle} onClick={this.handleClose.bind(this)}></MenuItem>

                                    </Menu>
                                </div>
                            </Drawer>
                        </div>
                    </MuiThemeProvider >
                </nav>
                {/* // Sppech screen common */}
                <div className="search-screen" id="search-screen" ref="searchRef">

                    <div className="search_bar">
                        <input style={container} type="text" className="form-control" placeholder="Search keyword, Supplies etc.."
                            ref="interim" />
                        {this.state.showResults ? <div> <img src='images/voice-search.jpg' className="search_image" onClick={this.toggleListen} /></div> : <div><img src="images/voicerecogniton2.gif" className="gif_align" /></div>}

                    </div>

                </div>
            </div>
        );
    }
}
const styleDivider = {
    float: 'left',
};

const divStyle = {
    width: 35,
    margin: '10px',
    height: 30,
};
const divStyle1 = {
    margin: '10px',
    height: 30,
};

const imageStyle = {
    width: 25,
    height: 25,
    marginRight: '10px',
    // marginLeft: '10px',
};

const menuTextStyle = {
    fontSize: 17,
    textAlignVertical: 'auto',
    border: '5px',
};

const logostyle = {
    height: 40,
    marginLeft: '40px',
}
NavDrawer.contextTypes = {
    router: PropTypes.object.isRequired
};


// Speech Screen style

const styleSpeech = {
    container: {
        fontSize: '10dp',
        width: '100%',
        marginTop: '30px',
        textAllign: 'center',
        marginBottom: '30px',
    },

}

const { container } = styleSpeech
export default NavDrawer;