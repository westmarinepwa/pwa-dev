import React, { Component } from 'react';

import NavDrawer from './NavDrawer.component.js';
import { Carousel } from 'react-responsive-carousel';
import PropTypes from 'prop-types';
import './App.css';
import logo from './west-marine-logo.png';

import Tabs from 'muicss/lib/react/tabs';
import Tab from 'muicss/lib/react/tab';


import injectTapEventPlugin from 'react-tap-event-plugin';
var loadjs = require('loadjs');



var router = require('react-router');
injectTapEventPlugin();

var back = localStorage.getItem('back')

class Dashboard extends Component {
	constructor() {
		super()
		
	}

	componentWillMount(){
		this.context.router.history.index=0
	}

	componentWillMount() {
		loadjs('/js/custom1.js');
		loadjs('/js/mobile-touch.js');

	}
	render() {

		return (
			<div className="homeMain">
				<div className="bgimg">
					<div className="topCenterLogo" >
						<img src={logo} className="responsive" alt="logo" width="756" height="147" />
					</div>
					<div className="loader"></div>
				</div>

				< div className="home">

					<NavDrawer />
					<div className="main-content" id='mainDiv'>
						<Carousel showThumbs={false} interval={10000} stopOnHover={true} autoPlay={true} showArrows={false} showStatus={false} showIndicators={false} infiniteLoop={true}>
							<div>
								<img src="images/slider1.png" alt="slider1" />

							</div>
							<div>
								<img src="images/slider2.png" alt="slider2" />

							</div>
							<div>
								<img src="images/slider3.png" alt="slider3" />

							</div>
						</Carousel>
						<div className="box-carousel">


							<div className="carousel-data">

								<img src="images/tile-back.png" alt="" title="" />
								<div className="carousel-arrow">
									<div className="left" id="previous">
										<img src="images/arrow-left.svg" alt="" />
									</div>
									<div className="right" id="next">
										<img src="images/arrow-right.svg" alt="" />
									</div>
								</div>
								<div className="box box1" id="box1">
									<img src="images/watersport-icon.svg" alt="" id="image1" />
									<span id="1">Water Sports</span>

								</div>
								<div className="midl" >
									<div className="box2 box" id='box2' >
										<img src="images/motors-icon.svg" alt="" id="image2" />
										<span id="2">Boats Motor & Parts</span>

									</div>

									<div className="box3 box" id='box3'>
										<img src="images/fishing-icon.svg" alt="" id="image3" />
										<span id="3">Fishing</span>

									</div>
								</div>
								<div className="box4 box" id='newProduct' >
									<img src="images/star-icon.svg" alt="" />
									<span id="4">New Products</span>

								</div>

							</div>
						</div>


						<div className="product-screen">
							<div className="product-scrn-wrap">

								<div>
									<div className="prod-tabs">
										<div className="tabs-justified">
											<p className="prod-str"><img src="images/star-icon.svg" /></p>
											<Tabs justified={true} onChange={this.onChange} defaultSelectedIndex={1} >

												<Tab value="pane-1" label='FISHING'> </Tab>
												<Tab value="pane-2" label='NEW PRODUCTS'></Tab>
												<Tab value="pane-3" label='WATER SPORTS'></Tab>
											</Tabs>
										</div>

									</div>
									<div className="prod-details">
										<div className="main-cat">
											<div className="electronic cat" >
												<a className="cat-link electronic-link" href="#" >
													<img src="images/prod_images/electrncs.png" />
												</a>
											</div>

											<div className="horz-cat">
												<div className="men cat left">
													<a className="cat-link men-link" href="#">
														<img src="images/prod_images/mens.png" />
													</a>
												</div>

												<div className="women cat right">
													<a className="cat-link women-link" href="#">
														<img src="images/prod_images/womens.png" />
													</a>
												</div>
											</div>

										</div>

										<div className="brands-cat">
											<p className="brands-hdng">Brands</p>
											<div className="brands-cat-gp1">
												<div className="air-hd cat">
													<a className="cat-link air-hd-link" href="#">
														<img src="images/prod_images/sprts-grp.png" />
													</a>
												</div>

												<div className="horz-cat">
													<div className="pelagic cat left">
														<a className="cat-link pelagic-link" href="#">
															<img src="images/prod_images/pelagic.png" />
														</a>
													</div>

													<div className="costa cat right">
														<a className="cat-link costa-link" href="#">
															<img src="images/prod_images/costa.png" />
														</a>
													</div>
												</div>
											</div>

											<div className="brands-cat-gp2">
												<div className="shimano cat">
													<a className="cat-link shimano-link" href="#">
														<img src="images/prod_images/shimano.png" />
													</a>
												</div>

												<div className="horz-cat">
													<div className="simrad cat left">
														<a className="cat-link simrad-link" href="#">
															<img src="images/prod_images/simrad.png" />
														</a>
													</div>

													<div className="wst-marn cat right">
														<a className="cat-link wst-marn-link" href="#">
															<img src="images/prod_images/wst-marn.png" />
														</a>
													</div>
												</div>
											</div>

											<div className="brands-cat-gp3">
												<div className="adidas cat">
													<a className="cat-link adidas-link" href="#">
														<img src="images/prod_images/adidas.png" />
													</a>
												</div>

												<div className="horz-cat">
													<div className="men cat left">
														<a className="cat-link affico-link" href="#">
															<img src="images/prod_images/affico.png" />
														</a>
													</div>

													<div className="lowrance cat right">
														<a className="cat-link lowrance-link" href="#">
															<img src="images/prod_images/lowrance.png" />
														</a>
													</div>
												</div>

											</div>


										</div>

									</div>


								</div>
								<div className="vw-all-wrap">
									<p className="vw-all">View All</p>
								</div>
							</div>
						</div>

					</div>


				</div>

			</div>


		);
	}

}


export default Dashboard



