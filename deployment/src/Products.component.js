import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import NavDrawer from './NavDrawer.component.js';
import Tabs from 'muicss/lib/react/tabs';
import Tab from 'muicss/lib/react/tab';

// import "/muicss/lib/sass/mui.scss";
// import "/muicss/lib/sass/mui/_colors";
var loadjs = require('loadjs');

class Products extends Component {
    onChange(i, value, tab, ev)  {
		   console.log(arguments);

   };
   componentWillMount() {
	loadjs('/js/custom1.js');

   }
    constructor(props) {
        super(props)
      }
    render() {
            return (
            <div className='page' id='prod-detail'>
                <NavDrawer />
             
                <div className="main-content" id='mainDiv'>
                <div className="pd-mainWrapper">
						<input type="text" placeholder="Search keywords, supplies etc" className="pd-searchtextBox" />

						<div className="pd-searchByBlock">
							<div className="pd-searchByBlock-left">
								<p>Did you mean</p>
								<p className="pd-searchKeywords">"Kayak"</p>
							</div>
							
							<div className="pd-searchByBlock-right">
								<img src="images/image-search-icon.svg" />
								<img src="images/voice-icon.svg" />
							</div>
							<ul className="sorting">
								<a href="#">
									<li className="active">westmarine.com</li>
								</a>
								<a href="#">
									<li>At your store</li>
								</a>
							</ul>
						
						<br clear="all" />
						<p className="pd-resultNumber">553 results found</p>
						<ul className="clr-plt">
							<p>Choose color</p>
							<li className="blck"></li>
							<li className="wht"></li>
							<li className="blu"></li>
							<li className="red"></li>
							<li className="orng"></li>
							<li className="prpl"></li>
						</ul>
						<br clear="all" />
						
						</div>
						<div className="pd-searchResult">

							
							<br clear="all" />

							<div className="pd-productBlock">
								<h1>Saba 14.0 Tandem Sit-Inside Kayak</h1>
								<div className="pd-rating">
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star-grey.png" className="rateStar" />
								</div>
								<br clear="all" />
								<div className="pd-productThumbWrapper">
									<img src="images/tile-one.png" className="pd-productThumb" />
									<br clear="all" />
									<img src="images/wishlistIcon_off.png" className="pd-wishlist" />
								</div>
								<div className="pd-productCategoryContent">
									<ul className="pd-productCategory">
										<li>
											<img src="images/size-icon.png" />
											<span>14</span>
										</li>

										<li>
											<img src="images/weight-icon2.png" />
											<span>90 LBS</span>
										</li>
										<li>
											<img src="images/person-icon.png" />
											<span>1 Person</span>
										</li>
									</ul>

								</div>

								<div className="pd-noteQtyLeft">
									Free shipping to store  </div>

								<div className="pd-noteQtyRight">
									<div className="stepper-widget">
										<input type="text" className="js-qty-input form-control quantity" value="1" />
										<button type="button" className="js-qty-up  btnSpinner">
											<img src="images/arrowUp.png" />
										</button>
										<button type="button" className="js-qty-down btnSpinner">
											<img src="images/arrowDown.png" />
										</button>

									</div>
									<br clear="all" />

								</div>
								<input type="button" value="BUY $399.00" className="pd-buyButton" />

								<br clear="all" />
							</div>

							<div className="pd-productBlock">
								<h1>Coast XT Sit-Inside Folding Kayak</h1>
								<div className="pd-rating">
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star-grey.png" className="rateStar" />
								</div>
								<br clear="all" />
								<div className="pd-productThumbWrapper">
									<img src="images/tile-four.png" className="pd-productThumb" />
									<br clear="all" />
									<img src="images/wishlistIcon_off.png" className="pd-wishlist" />
								</div>
								<div className="pd-productCategoryContent">
									<ul className="pd-productCategory">
										<li>
											<img src="images/size-icon.png" />
											<span>14</span>
										</li>

										<li>
											<img src="images/weight-icon2.png" />
											<span>90 LBS</span>
										</li>
										<li>
											<img src="images/person-icon.png" />
											<span>1 Person</span>
										</li>
									</ul>

								</div>

								<div className="pd-noteQtyLeft">
									Free shipping to store</div>

								<div className="pd-noteQtyRight">
									<div className="stepper-widget">
										<input type="text" className="js-qty-input form-control quantity" value="1" />
										<button type="button" className="js-qty-up  btnSpinner">
											<img src="images/arrowUp.png" />
										</button>
										<button type="button" className="js-qty-down btnSpinner">
											<img src="images/arrowDown.png" />
										</button>

									</div>
									<br clear="all" />

								</div>
								<input type="button" value="BUY $399.00" className="pd-buyButton" />

								<br clear="all" />
							</div>

							<div className="pd-productBlock">
								<h1>Island Voyage™ 2 Inflatable Kayak</h1>
								<div className="pd-rating">
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star-grey.png" className="rateStar" />
								</div>
								<br clear="all" />
								<div className="pd-productThumbWrapper">
									<img src="images/tile-two.png" className="pd-productThumb" />
									<br clear="all" />
									<img src="images/wishlistIcon_off.png" className="pd-wishlist" />
								</div>
								<div className="pd-productCategoryContent">
									<ul className="pd-productCategory">
										<li>
											<img src="images/size-icon.png" />
											<span>14</span>
										</li>

										<li>
											<img src="images/weight-icon2.png" />
											<span>90 LBS</span>
										</li>
										<li>
											<img src="images/person-icon.png" />
											<span>1 Person</span>
										</li>
									</ul>

								</div>

								<div className="pd-noteQtyLeft">Free shipping to store</div>

								<div className="pd-noteQtyRight">
									<div className="stepper-widget">
										<input type="text" className="js-qty-input form-control quantity" value="1" />
										<button type="button" className="js-qty-up  btnSpinner">
											<img src="images/arrowUp.png" />
										</button>
										<button type="button" className="js-qty-down btnSpinner">
											<img src="images/arrowDown.png" />
										</button>

									</div>
									<br clear="all" />

								</div>
								<input type="button" value="BUY $399.00" className="pd-buyButton" />

								<br clear="all" />
							</div>


							<div className="pd-productBlock">
								<h1>AdvancedFrame™ Folding Inflatabl…</h1>
								<div className="pd-rating">
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star.png" className="rateStar" />
									<img src="images/star-grey.png" className="rateStar" />
								</div>
								<br clear="all" />
								<div className="pd-productThumbWrapper">
									<img src="images/tile-three.png" className="pd-productThumb" />
									<br clear="all" />
									<img src="images/wishlistIcon_on.png" className="pd-wishlist" />
								</div>
								<div className="pd-productCategoryContent">
									<ul className="pd-productCategory">
										<li>
											<img src="images/size-icon.png" />
											<span>14</span>
										</li>

										<li>
											<img src="images/weight-icon2.png" />
											<span>90 LBS</span>
										</li>
										<li>
											<img src="images/person-icon.png" />
											<span>1 Person</span>
										</li>
									</ul>

								</div>

								<div className="pd-noteQtyLeft">Free shipping to store</div>

								<div className="pd-noteQtyRight">
									<div className="stepper-widget">
										<input type="text" className="js-qty-input form-control quantity" value="1" />
										<button type="button" className="js-qty-up  btnSpinner">
											<img src="images/arrowUp.png" />
										</button>
										<button type="button" className="js-qty-down btnSpinner">
											<img src="images/arrowDown.png" />
										</button>

									</div>
									<br clear="all" />

								</div>
								<input type="button" value="BUY $399.00" className="pd-buyButton" />

								<br clear="all" />
							</div>
							<p className="view-more">Load More</p>

						</div>

					</div>



				</div>

                
            </div>
        );
    }
}
export default Products;