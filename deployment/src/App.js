import React, { Component } from 'react';
import {Router, Route, browserHistory, applyRouterMiddleware} from 'react-router';
import './App.css';
import Home from './home.component.js'
import Dashboard from './dashboard.component.js'
import Products from './Products.component.js'


const routes = (
  <Route>
   <Route path="/" component={Dashboard} />
        {/* <Route path="/dashboard" component={Dashboard}/> */}
        <Route path="/Products" component={Products}/>
  </Route>
);

class App extends Component {

   render() {
     return (
       <Router history={browserHistory}>
        {routes}
       </Router>
     );
   }
 }

/* class App extends Component {

  render() {
    return (
      <Router history={browserHistory}>
       {routes}
      </Router>
    );
  }
} */

// class App extends Component {

//   render() {
//     return (
//       <Router

//       history={browserHistory}
      
//       render={applyRouterMiddleware(useTransitions({
//         TransitionGroup: CSSTransitionGroup,
//         onShow(prevState, nextState, replaceTransition) {
//           replaceTransition({
//             transitionName: `show-${nextState.children.props.route.transition}`,
//             transitionEnterTimeout: 500,
//             transitionLeaveTimeout: 300,
//           });
//         },
//         onDismiss(prevState, nextState, replaceTransition) {
//           replaceTransition({
//             transitionName: `dismiss-${prevState.children.props.route.transition}`,
//             transitionEnterTimeout: 500,
//             transitionLeaveTimeout: 300,
//           });
//         },
//         defaultTransition: {
//           transitionName: 'fade',
//           transitionEnterTimeout: 20,
//           transitionLeaveTimeout: 20,
//         }
//       }))}
//     >  
//      {routes}
//     </Router>
//     );
//   }
// }



 export default App;

